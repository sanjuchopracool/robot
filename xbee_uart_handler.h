#ifndef XBEEUARTHANDLER_H
#define XBEEUARTHANDLER_H

#include <stdint.h>
#include <stm32f4xx_hal.h>
#include <stm32f4xx_hal_uart.h>

#define TX_BUFFER_SIZE 1024
class XbeeUartHandler
{
public:
    XbeeUartHandler();
    ~XbeeUartHandler();

    void init();
    bool canTransfer(uint16_t numBytes) const;
    void addToBuffer(const uint8_t* buff, uint16_t numBytes);
    void startReceiving();

    friend void HAL_UART_TxCpltCallback(UART_HandleTypeDef *UartHandle);
private:
    int16_t usedBufferSize() const;
    void startTransfer();

private:
    int16_t m_txHead;
    int16_t m_txTail;
    uint16_t m_currentTransferSize;
    __IO ITStatus m_TransferComplete;
    __IO uint8_t txBuffer[TX_BUFFER_SIZE];
};

#endif // XBEEUARTHANDLER_H
