#ifndef SETDIRECTIONCOMMAND_H
#define SETDIRECTIONCOMMAND_H

#include "../../STM32Cube/Repository/STM32Cube_FW_F4_V1.6.0/chopsProjects/STM32F401RE-Nucleo/Examples/GPIO/GPIO_IOToggle-cpp/abstract_transfer.h"

class SetDirectionCommand: public AbstractTransfer
{
public:
    SetDirectionCommand();
    ~SetDirectionCommand();

    Direction direction() const { return m_direction ;}
    void setDirection(Direction);

private:
    Direction m_direction;
};

#endif // SETDIRECTIONCOMMAND_H
