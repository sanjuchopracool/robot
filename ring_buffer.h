#ifndef RINGBUFFER_H
#define RINGBUFFER_H

#include "my_container_utility_function.h"
#include <stdint.h>

template < typename T, uint32_t N>
class RingBuffer
{
    typedef std::size_t     size_type;
    static_assert(is_power_of_two<N>::value, "Ring buffer Size should be power of two!!!! eg( 4, 8, 16, 2^n)");

public:
    RingBuffer() : m_head(0), m_tail(0), m_capacity(N), full(false) {

    }

    ~RingBuffer() {

    }

    inline size_type capacity() const noexcept {
        return m_capacity;
    }


    bool isEmpty() const noexcept {
        return ((m_head == m_tail) && !full);
    }

    bool isFull() const noexcept {
        return ((m_head == m_tail) && full);
    }

    size_type usedSize() const noexcept {
        if (m_head == m_tail)
        {
            if (full)
                return m_capacity;

            return 0;
        }

        return ((m_capacity + m_head - m_tail) & (m_capacity - 1));
    }

    size_type unusedSize() const noexcept {
        if (m_head == m_tail)
        {
            if (full)
                return 0;

            return m_capacity;
        }

        return (m_capacity -((m_capacity + m_head - m_tail) & (m_capacity - 1)));
    }

    size_type insert(const T* src, size_type numBytes) noexcept {
        if ((m_head == m_tail) && full)
            return 0;

        for (size_type i = 0; i < numBytes; i++)
        {
            m_buffer[m_head] = src[i];
            m_head = (m_head + 1) & (m_capacity -1);
            if (m_head == m_tail)
            {
                full = true;
                return (i + 1);
            }
        }
        return numBytes;
    }

    size_type remove(T* dest, size_type numBytes) noexcept {
        if ((m_head == m_tail) && !full)
            return 0;

        for (size_type i = 0; i < numBytes; i++)
        {
            dest[i] = m_buffer[m_tail];
            m_tail = (m_tail + 1) & (m_capacity -1);
            if (m_tail == m_head)
            {
                full = false;
                return (i + 1);
            }
        }
        return numBytes;
    }

    const T& operator[] (size_type index) {
        index = (m_tail + index) & (m_capacity - 1);
        return m_buffer[index];
    }

    T pop() {
        T t = m_buffer[m_tail];
        m_tail = (m_tail + 1) & (m_capacity - 1);
        return t;
    }

    void push(const T& t) {
        m_buffer[m_head] = t;
        m_head = (m_head + 1) & (m_capacity - 1);
    }

protected:
    T m_buffer[N];
    size_type m_head;
    size_type m_tail;
    size_type m_capacity;
    bool full;
};

#endif // RINGBUFFER_H
