#include "setspeed_command.h"

SetSpeedCommand::SetSpeedCommand()
{
    m_speed = 0;
    setInternalCall(static_cast<uint8_t>(METHODC_CALL_TYPE::SET_SPEED));
}

SetSpeedCommand::~SetSpeedCommand()
{
}

void SetSpeedCommand::setSpeed(uint16_t speed)
{
    if(m_speed != speed)
    {
        setData(&m_speed, sizeof(m_speed));
        m_speed = speed;
        transfer();
    }
}

