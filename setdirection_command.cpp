#include "setdirection_command.h"
#include <qdebug.h>
SetDirectionCommand::SetDirectionCommand()
{
    setInternalCall(static_cast<uint8_t>(METHODC_CALL_TYPE::SET_DIRECTION));
}

SetDirectionCommand::~SetDirectionCommand()
{

}

void SetDirectionCommand::setDirection(Direction direction)
{
    if(m_direction != direction)
    {
        m_direction = direction;
        setData(&m_direction, sizeof(m_direction));
        qDebug() << (int)m_direction;
        transfer();
    }
}

