#include "command_executer.h"
#include "xbee_uart_handler.h"

extern CommandExecuter commandBuffer;
CommandExecuter::CommandExecuter()
{
    for (int i = 0 ;i < bufferSize; i++)
        m_freeIndexes.push(i);
}

bool CommandExecuter::isFull() const
{
    return m_commandsToExecute.isFull();
}

bool CommandExecuter::isEmpty() const
{
    return m_commandsToExecute.isEmpty();
}

AbstractTransfer &CommandExecuter::nextCommand()
{
    return m_commands[m_freeIndexes.pop()];
}

void CommandExecuter::appendCommand(const AbstractTransfer &command)
{
    volatile uint8_t index = &command - m_commands;
    m_commandsToExecute.push((uint8_t)index);
}

void CommandExecuter::executeNextCommand()
{
    if(!m_commandsToExecute.isEmpty())
    {
        AbstractTransfer& commandToExec = m_commands[m_commandsToExecute.pop()];
        commandToExec.respond();
        m_freeIndexes.push(&commandToExec - m_commands); //append to free buffer
    }
}


bool PriorityComparator::operator ()(uint16_t i, uint16_t j) const
{
    //checked enum class by default provide < and > operator and works like bormal numbers
    return ((commandBuffer.m_commands[i].priority()) < (commandBuffer.m_commands[j].priority()));
}
