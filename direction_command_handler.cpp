#include "direction_command_handler.h"
#include "motor_driver.h"
extern MotorDriver motorDriver;
DirectionCommandHandler::DirectionCommandHandler()
{

}

void DirectionCommandHandler::setDirection(Direction direction)
{
    float val = 100;
    switch (direction) {
    case Direction::Up:
        motorDriver.setMotorSpeeds(val, val);
        break;
    case Direction::Left:
        motorDriver.setMotorSpeeds(-val, val);
        break;
    case Direction::Down:
        motorDriver.setMotorSpeeds(-val, -val);
        break;
    case Direction::Right:
        motorDriver.setMotorSpeeds(val, -val);
        break;
    default:
        motorDriver.setMotorSpeeds(0, 0);
        break;
    }
}

