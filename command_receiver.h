#ifndef COMMANDRECEIVER_H
#define COMMANDRECEIVER_H
#include <cstdint>
#include "stm32f4xx_hal.h"
#include "ring_buffer.h"

class CommandReceiver :
        public RingBuffer<uint8_t, 1024>
{
    enum class PatternStatus : uint8_t {
        START_NOT_FOUND,
        START_FOUND,
        END_FOUND,
        MULTIPLE_START
    };

public:
    void checkForReceivedPacket();

private:
    void parseData();

private:
    __IO PatternStatus m_currentStatus;
};

#endif // COMMANDRECEIVER_H
