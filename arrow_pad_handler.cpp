#include "arrow_pad_handler.h"
#include <QDebug>

QStringList directionStrings;
ArrowPadHandler::ArrowPadHandler(QObject *parent)
    : QObject(parent)
{
    m_direction = Direction::None;
    directionStrings.append("Up");
    directionStrings.append("Left");
    directionStrings.append("Right");
    directionStrings.append("down");
    directionStrings.append("none");
    connect(this, SIGNAL(directionChanged()), this, SLOT(onDirectionChanged()));
}

ArrowPadHandler::~ArrowPadHandler()
{

}

bool ArrowPadHandler::keyPressEvent(int modifier, int key, bool autoRepeat)
{
    if (modifier == Qt::NoModifier && !autoRepeat)
    {
        Direction direction = Direction::None;
        if (key == Qt::Key_Up)
            direction = Direction::Up;
        else if (key == Qt::Key_Down)
            direction = Direction::Down;
        else if (key == Qt::Key_Left)
            direction = Direction::Left;
        else if (key == Qt::Key_Right)
            direction = Direction::Right;

        if (direction != Direction::None && m_direction != direction)
        {
            m_direction = direction;
            directionChanged();
            return true;
        }
    }

    return false;
}

bool ArrowPadHandler::keyReleaseEvent(int modifier, int key, bool autoRepeat)
{
    if (modifier == Qt::NoModifier && !autoRepeat)
    {
        if (key == Qt::Key_Up ||
                key == Qt::Key_Down ||
                key == Qt::Key_Left ||
                key == Qt::Key_Right)
        {
            if ( m_direction != Direction::None)
            {
                m_direction = Direction::None;
                emit directionChanged();
                return true;
            }
        }
    }

    return false;
}

void ArrowPadHandler::onDirectionChanged()
{
    directionCommand.setDirection(m_direction);
}

