#include "abstract_transfer.h"
#include <cstring>
#include "transfer_functions.h"
#include "command_packet_contants.h"

#ifdef DESKTOP_APP
#include <QSerialPort>
#include <qdebug.h>
extern QSerialPort myPort;
#else
#include "xbee_uart_handler.h"
#include "direction_command_handler.h"
extern XbeeUartHandler xbee;
extern DirectionCommandHandler directionHandler;
#endif


AbstractTransfer::AbstractTransfer()
{
}

void AbstractTransfer::loadFromBytes(uint8_t *data, uint16_t startIndex, uint8_t size, uint32_t bufferSize)
{
    if(data)
    {
        constexpr uint8_t sizeOfInternalData = sizeof(AbstractTransferData);
        uint8_t tempData[sizeOfInternalData];
        for(int i =0;i < sizeOfInternalData; i++)
        {
            tempData[i] = data[(startIndex + i) & (bufferSize -1)];
        }

        std::memcpy(&d , &tempData, sizeOfInternalData);
        //copy res of data
        m_size = size - sizeOfInternalData;
        uint32_t index = (startIndex + sizeOfInternalData) & (bufferSize - 1);
        for (int i = 0; i < m_size; i++)
        {
            m_data[i] = data[(index + i) & (bufferSize - 1)];
        }
    }
}

AbstractTransfer::~AbstractTransfer()
{
}

void AbstractTransfer::setId(uint32_t id)
{
    d.id = id;
}

uint32_t AbstractTransfer::id() const
{
    return d.id;
}

void AbstractTransfer::setDataLength(uint32_t dataLength)
{
    d.dataLength = dataLength;
}

uint32_t AbstractTransfer::dataLength() const
{
    return d.dataLength;
}

void AbstractTransfer::setTransferType(TransferType type)
{
    d.type = type;
}

TransferType AbstractTransfer::transferType() const
{
    return d.type;
}

void AbstractTransfer::setPriority(TransferPriority priority)
{
    d.priority = priority;
}

TransferPriority AbstractTransfer::priority() const
{
    return d.priority;
}

void AbstractTransfer::setInternalCall(uint8_t internalType)
{
    d.internalType = internalType;
}

uint8_t AbstractTransfer::internalCall() const
{
    return d.internalType;
}

void AbstractTransfer::setData(void *data, uint8_t size)
{
    memcpy(m_data, data, size);
    m_size = size;
}

void AbstractTransfer::transfer()
{
    //NOTE every time get a new id
    d.id = uniqueId();
    //put into the transfer priority queue
#ifdef DESKTOP_APP
    myPort.write(packetStart, PACKET_START_SIZE);
    myPort.write((char*)&d, sizeof(d));
    myPort.write((char*)m_data, m_size);
    myPort.write(packetEnd, PACKET_END_SIZE);
#endif
}

void AbstractTransfer::respond()
{
    if(d.type == TransferType::METHOD_CALL)
    {
        // use kind of function mapper with
        //respond and write back
        // make sure you don,t change id
#ifndef DESKTOP_APP
        METHODC_CALL_TYPE methodCall = static_cast<METHODC_CALL_TYPE>(d.internalType);
        switch (methodCall) {
        case METHODC_CALL_TYPE::SET_DIRECTION:
        {
            directionHandler.setDirection(static_cast<Direction>(m_data[0]));
            break;
        }
        default:
            break;
        }
#endif
    }
}

void AbstractTransfer::execute()
{
    //execute
}
