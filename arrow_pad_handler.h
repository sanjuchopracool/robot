#ifndef ARROWPADHANDLER_H
#define ARROWPADHANDLER_H

#include <QObject>
#include <QKeyEvent>
#include "../WirelessTransfer/setdirection_command.h"
#include "../../STM32Cube/Repository/STM32Cube_FW_F4_V1.6.0/chopsProjects/STM32F401RE-Nucleo/Examples/GPIO/GPIO_IOToggle-cpp/command_packet_contants.h"
#include "../../STM32Cube/Repository/STM32Cube_FW_F4_V1.6.0/chopsProjects/STM32F401RE-Nucleo/Examples/GPIO/GPIO_IOToggle-cpp/transfer_constants.h"

class ArrowPadHandler : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool up READ isUp NOTIFY directionChanged)
    Q_PROPERTY(bool down READ isDown NOTIFY directionChanged)
    Q_PROPERTY(bool left READ isLeft NOTIFY directionChanged)
    Q_PROPERTY(bool right READ isRight NOTIFY directionChanged)
    Q_PROPERTY(bool none READ isNone NOTIFY directionChanged)

public:
    explicit ArrowPadHandler(QObject *parent = 0);
    ~ArrowPadHandler();

    inline bool isUp() const { return m_direction == Direction::Up;}
    inline bool isDown() const { return m_direction == Direction::Down;}
    inline bool isLeft() const { return m_direction == Direction::Left;}
    inline bool isRight() const { return m_direction == Direction::Right;}
    inline bool isNone() const { return m_direction == Direction::None; }

    Q_INVOKABLE bool keyPressEvent(int modifier, int key, bool autoRepeat);
    Q_INVOKABLE bool keyReleaseEvent(int modifier, int key, bool autoRepeat);

signals:
    void directionChanged();

private slots:
    void onDirectionChanged();

private:
    Direction m_direction;
    SetDirectionCommand directionCommand;
};

#endif // ARROWPADHANDLER_H
