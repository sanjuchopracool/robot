#ifndef MY_CONTAINER_UTILITY_FUNCTION
#define MY_CONTAINER_UTILITY_FUNCTION

template<uint32_t N>
struct is_power_of_two {
    enum {value = N && !(N & (N - 1))};
};

#endif // MY_CONTAINER_UTILITY_FUNCTION

