#ifndef DIRECTIONCOMMANDHANDLER_H
#define DIRECTIONCOMMANDHANDLER_H
#include "abstract_transfer.h"

class DirectionCommandHandler
{
public:
    DirectionCommandHandler();
    void setDirection(Direction direction);
};

#endif // DIRECTIONCOMMANDHANDLER_H
