#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include "arrow_pad_handler.h"
#include <QSerialPort>
#include <QDebug>

QSerialPort myPort;

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    ArrowPadHandler arrowHandler;
    QQmlApplicationEngine engine;
    myPort.setPortName("/dev/ttyUSB0");
    if(!myPort.open(QIODevice::WriteOnly))
        qDebug() << "error opening port!!!";
    engine.rootContext()->setContextProperty("arrowHandler", &arrowHandler);
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}
