#ifndef MOTORDRIVER_H
#define MOTORDRIVER_H
#include <stdint.h>

class MotorDriver
{
public:
    MotorDriver();
    ~MotorDriver();

    void init();
    void setMotorSpeeds(float speedA, float speedB);
    float getSpeedA() const;
    float getSpeedB() const;

private:
    void setSpeedAOC(uint32_t val);
    void setSpeedBOC(uint32_t val);

private:
    volatile float m_currentSpeedA;
    volatile float m_currentSpeedB;
};

#endif // MOTORDRIVER_H
