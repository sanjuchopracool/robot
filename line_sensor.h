#ifndef LINESENSOR_H
#define LINESENSOR_H


class LineSensor
{
public:
    LineSensor();
    ~LineSensor();
    void init();
    void startConversion();

};

#endif // LINESENSOR_H
