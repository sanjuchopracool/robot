TEMPLATE = app

QT += qml quick widgets serialport

SOURCES += main_app.cpp \
    arrow_pad_handler.cpp \
    setdirection_command.cpp \
    setspeed_command.cpp \
    abstract_transfer.cpp

RESOURCES += qml.qrc
CONFIG += C++14
DEFINES += DESKTOP_APP
# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)
HEADERS += \
    arrow_pad_handler.h \
    setdirection_command.h \
    setspeed_command.h \
    abstract_transfer.h
