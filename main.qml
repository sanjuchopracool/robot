import QtQuick 2.3
import QtQuick.Controls 1.2
import QtQuick.Dialogs 1.2

ApplicationWindow {
    visible: true
    title: "Blue sky dialog"
    width: mainRect.implicitWidth
    height: mainRect.implicitHeight
    Rectangle {
        id : mainRect
        implicitWidth: 400
        implicitHeight: 400
        anchors.fill: parent
        Rectangle {
            id : topRect
            width: parent.width / 3;
            height: width
            border.color: "blue";
            anchors.top: parent.top
            anchors.horizontalCenter: parent.horizontalCenter;
            color: arrowHandler.up ? "green" : "white"
        }

        Rectangle {
            id : leftRect
            width: parent.width / 3;
            height: width
            border.color: "blue";
            anchors.left: parent.left
            anchors.verticalCenter: parent.verticalCenter;
            color: arrowHandler.left ? "green" : "white"
        }

        Rectangle {
            id : bottomRect
            width: parent.width / 3;
            height: width
            border.color: "blue";
            anchors.bottom: parent.bottom
            anchors.horizontalCenter: parent.horizontalCenter;
            color: arrowHandler.down ? "green" : "white"
        }

        Rectangle {
            id : rightRect
            width: parent.width / 3;
            height: width
            border.color: "blue";
            anchors.right: parent.right
            anchors.verticalCenter: parent.verticalCenter;
            color: arrowHandler.right ? "green" : "white"
        }

        Rectangle {
            id : middleRect
            width: parent.width / 3;
            height: width
            border.color: "blue";
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter;
            color: arrowHandler.none ? "green" : "white"
        }

        focus: true
        Keys.onPressed: {
            if(arrowHandler.keyPressEvent(event.modifiers, event.key, event.isAutoRepeat))
                event.accepted = true;
        }

        Keys.onReleased: {
            if(arrowHandler.keyReleaseEvent(event.modifiers, event.key, event.isAutoRepeat))
                event.accepted = true;
        }
    }
}
