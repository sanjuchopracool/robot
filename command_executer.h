#ifndef COMMANDEXECUTER_H
#define COMMANDEXECUTER_H
#include "abstract_transfer.h"
#include "ring_buffer.h"
#include "priority_queue.h"
#include "command_receiver.h"

constexpr const int bufferSize = 64;
class PriorityComparator
{
public:
    bool operator () (uint16_t i, uint16_t j) const;
};

class CommandExecuter
{
    friend class CommandReceiver;
    friend class PriorityComparator;

public:
    CommandExecuter();

    bool isFull() const;
    bool isEmpty() const;
    void executeNextCommand();

private:
    AbstractTransfer &nextCommand();
    void appendCommand(const AbstractTransfer& command);

private:
    AbstractTransfer m_commands[bufferSize]; //i will keep a total of 256 command max
    RingBuffer<uint8_t, bufferSize> m_freeIndexes;
    PriorityQueue<uint8_t, bufferSize, PriorityComparator> m_commandsToExecute;
};

#endif // COMMANDEXECUTER_H
