#ifndef PRIORITY_QUEUE
#define PRIORITY_QUEUE

#include <stdint.h>
#include <cstring>
#include <queue>
#include <algorithm>

template < typename T, uint16_t N, typename lessFunctor = std::less<T> >
class PriorityQueue
{
    typedef uint16_t     size_type;
    typedef lessFunctor  less;
    typedef int32_t      signed_size_type;
    static_assert(is_power_of_two<N>::value, "Ring buffer Size should be power of two!!!! eg( 4, 8, 16, 2^n)");
public:

    PriorityQueue() : m_size(0), m_tail(0) {}

    size_type capacity() const noexcept {
        return N;
    }

    size_type usedSize() const noexcept {
        return m_size;
    }

    size_type ununsedSize() const noexcept {
        return (N - m_size);
    }

    bool isFull() const noexcept {
        return (m_size == N);
    }

    bool isEmpty() const noexcept {
        return (m_size == 0);
    }

    T pop () {
        T t = m_buffer[m_tail];
        m_tail = (m_tail + 1) & (N -1);
        m_size--;
        return t;
    }

    void push (const T& t) {
        if(!m_size)
        {
            m_buffer[m_tail] = t;
        }
        else
        {
            signed_size_type appendIndex = (m_tail + m_size) & (N-1);
            m_buffer[appendIndex] = t;
            signed_size_type prevIndex = (appendIndex > 0) ? (appendIndex -1) : (N-1);
            for (size_t i = 0; i < m_size && m_less(m_buffer[prevIndex], m_buffer[appendIndex]); i++)
            {
                std::swap(m_buffer[prevIndex], m_buffer[appendIndex]);
                prevIndex = (prevIndex > 0) ? (prevIndex -1) : (N-1);
                appendIndex = (prevIndex + 1) & (N-1);
            }
        }

        m_size++;
    }

private:
    T m_buffer[N];
    size_type m_size;
    size_type m_tail;
    less m_less;
};

#endif // PRIORITY_QUEUE

