#include "motor_driver.h"
#include "main.h"
#include "xbee_uart_handler.h"
extern XbeeUartHandler xbee;
uint8_t left[] ="L\n";
uint8_t right[] ="R\n";
extern "C"
{
#include <stm32f4xx_hal.h>
#include <stm32f4xx_nucleo.h>
#include <stm32f4xx_hal_tim.h>
#include <math.h>
}

#define MOTORSHIELD_IN1     PA9
#define MOTORSHIELD_IN2     PA7
#define MOTORSHIELD_IN3     PA6
#define MOTORSHIELD_IN4     PA5
#define SPEEDPIN_A          PC7 // PWM3/2
#define SPEEDPIN_B          PB6 // PWM4/1
#define PERIOD_VALUE  (1000 - 1) //i will directly multiply the float value with 10 and use it

static TIM_HandleTypeDef  Tim3Handle;
static TIM_HandleTypeDef  Tim4Handle;
static TIM_OC_InitTypeDef sConfig;

static uint8_t oppositeSigns(int x, int y)
{
    return ((x ^ y) < 0);
}

MotorDriver::MotorDriver()
    : m_currentSpeedA(0),
      m_currentSpeedB(0)
{

}

MotorDriver::~MotorDriver()
{

}

void MotorDriver::init()
{
    //init direction control pins
    __GPIOA_CLK_ENABLE();

    GPIO_InitTypeDef  GPIO_InitStruct;
    GPIO_InitStruct.Pin = GPIO_PIN_9 | GPIO_PIN_7 | GPIO_PIN_6 | GPIO_PIN_5 ;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FAST;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    SystemCoreClockUpdate();
    //Timer related stuff
    Tim3Handle.Instance = TIM3;
    /* Compute the prescaler value to have TIM3 counter clock equal to 20 MHz */
    uint32_t uhPrescalerValue = (uint32_t) ((SystemCoreClock /2) / 21000000) - 1; //17
    Tim3Handle.Init.Prescaler = uhPrescalerValue;
    Tim3Handle.Init.Period = PERIOD_VALUE;
    Tim3Handle.Init.ClockDivision = 0;
    Tim3Handle.Init.CounterMode = TIM_COUNTERMODE_UP;
    if(HAL_TIM_PWM_Init(&Tim3Handle) != HAL_OK)
    {
        /* Initialization Error */
        Error_Handler();
    }

    Tim4Handle.Instance = TIM4;
    Tim4Handle.Init.Prescaler = uhPrescalerValue;
    Tim4Handle.Init.Period = PERIOD_VALUE;
    Tim4Handle.Init.ClockDivision = 0;
    Tim4Handle.Init.CounterMode = TIM_COUNTERMODE_UP;
    if(HAL_TIM_PWM_Init(&Tim4Handle) != HAL_OK)
    {
        /* Initialization Error */
        Error_Handler();
    }

    /* Common configuration for all channels */
    sConfig.OCMode = TIM_OCMODE_PWM1;
    sConfig.OCPolarity = TIM_OCPOLARITY_HIGH;
    sConfig.OCFastMode = TIM_OCFAST_DISABLE;
//    setMotorSpeeds(0, 0);
    setMotorSpeeds(-1, -1);
}

void MotorDriver::setMotorSpeeds(float speedA, float speedB)
{
    volatile float spA = speedA;
    volatile float spB = speedB;
    volatile float absVal = fabsf(spA);
    if(absVal <= 100.0 && spA != m_currentSpeedA)
    {
        if(oppositeSigns(spA, m_currentSpeedA))
        {
            xbee.addToBuffer(left, 2);
            //change directions
            if(spA >= 0)
            {
                HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9, GPIO_PIN_SET);
                HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7, GPIO_PIN_RESET);
            }
            else
            {
                HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9, GPIO_PIN_RESET);
                HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7, GPIO_PIN_SET);
            }
        }

        //do the pwm stuff
        if(absVal != fabsf(m_currentSpeedA))
        {
            setSpeedAOC((uint32_t)(absVal*10));
        }

        m_currentSpeedA = spA;
    }

    absVal = fabsf(spB);
    if(absVal <= 100 && spB != m_currentSpeedB)
    {
        xbee.addToBuffer(right, 2);
        if(oppositeSigns(spB, m_currentSpeedB))
        {
            //change direction
            if(spB >= 0)
            {
                HAL_GPIO_WritePin(GPIOA, GPIO_PIN_6, GPIO_PIN_SET);
                HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, GPIO_PIN_RESET);
            }
            else
            {
                HAL_GPIO_WritePin(GPIOA, GPIO_PIN_6, GPIO_PIN_RESET);
                HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, GPIO_PIN_SET);
            }
        }

        //do the pwm stuff
        if(absVal != fabsf(m_currentSpeedB))
        {
            setSpeedBOC((uint32_t)(absVal*10));
        }
        m_currentSpeedB = spB;
    }
}

float MotorDriver::getSpeedA() const
{
    return m_currentSpeedA;
}

float MotorDriver::getSpeedB() const
{
    return m_currentSpeedB;
}

void MotorDriver::setSpeedAOC(uint32_t val)
{
    /* Set the pulse value for channel 1 */
    sConfig.Pulse = val;
    if(HAL_TIM_PWM_ConfigChannel(&Tim3Handle, &sConfig, TIM_CHANNEL_2) != HAL_OK)
    {
      /* Configuration Error */
      Error_Handler();
    }

    if(HAL_TIM_PWM_Start(&Tim3Handle, TIM_CHANNEL_2) != HAL_OK)
    {
      /* PWM Generation Error */
      Error_Handler();
    }
}

void MotorDriver::setSpeedBOC(uint32_t val)
{
    sConfig.Pulse = val;
    if(HAL_TIM_PWM_ConfigChannel(&Tim4Handle, &sConfig, TIM_CHANNEL_1) != HAL_OK)
    {
      /* Configuration Error */
      Error_Handler();
    }

    if(HAL_TIM_PWM_Start(&Tim4Handle, TIM_CHANNEL_1) != HAL_OK)
    {
      /* PWM Generation Error */
      Error_Handler();
    }
}

void HAL_TIM_PWM_MspInit(TIM_HandleTypeDef *htim)
{
    GPIO_InitTypeDef   GPIO_InitStruct;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
    /*##-1- Enable peripherals and GPIO Clocks #################################*/
    /* TIMx Peripheral clock enable */
    if(htim->Instance == TIM3)
    {
        __TIM3_CLK_ENABLE();
        __GPIOC_CLK_ENABLE();
        GPIO_InitStruct.Alternate = GPIO_AF2_TIM3;
        GPIO_InitStruct.Pin = GPIO_PIN_7;
        HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
    }

    if(htim->Instance == TIM4)
    {
        __TIM4_CLK_ENABLE();
        __GPIOB_CLK_ENABLE();
        GPIO_InitStruct.Alternate = GPIO_AF2_TIM4;
        GPIO_InitStruct.Pin = GPIO_PIN_6;
        HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
    }
}
