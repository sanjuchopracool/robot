#ifndef SETSPEEDCOMMAND_H
#define SETSPEEDCOMMAND_H

#include "abstract_transfer.h"

class SetSpeedCommand: public AbstractTransfer
{
public:
    SetSpeedCommand();
    ~SetSpeedCommand();

    inline uint16_t speed() const { return m_speed; }
    void setSpeed(uint16_t speed);
private:
    uint16_t m_speed;
};

#endif // SETSPEEDCOMMAND_H
