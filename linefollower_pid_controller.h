#ifndef LINEFOLLOWERPIDCONTROLLER_H
#define LINEFOLLOWERPIDCONTROLLER_H
#include "main.h"

class LineFollowerPIDController
{
public:
    LineFollowerPIDController();
    void updateValues(uint16_t * values);

private:
    float m_prop;
    float m_diff;
    float m_integral;
    float sum;
};

#endif // LINEFOLLOWERPIDCONTROLLER_H
