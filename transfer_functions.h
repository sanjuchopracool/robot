#ifndef TRANSFER_FUNCTIONS
#define TRANSFER_FUNCTIONS

#include <cstdint>

static uint32_t idCounter = 1;
static uint32_t uniqueId()
{
    return idCounter++;
}

#endif // TRANSFER_FUNCTIONS

