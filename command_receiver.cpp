#include "command_receiver.h"
#include "command_packet_contants.h"
#include "command_executer.h"

extern CommandExecuter commandBuffer;

void CommandReceiver::checkForReceivedPacket()
{
    if(this->usedSize() > (PACKET_START_SIZE + PACKET_END_SIZE)) // abstract transfer d size is 8
        parseData();
}

void CommandReceiver::parseData()
{
    findStartPattern:
    //match the start pattern else remove the bytes
    if(m_currentStatus == PatternStatus::START_NOT_FOUND)
    {
        int16_t receivedSize = (m_head - m_tail + m_capacity) & (m_capacity - 1);
        for(int i = 0; i < receivedSize; i++)
        {
            if(m_buffer[m_tail] == packetStart[0])
            {
                //first character is matching, match other characters
                int j = 1;
                for(; j < PACKET_START_SIZE; j++)
                {
                    if(m_buffer[m_tail + j] != packetStart[j])
                        break; //inner loop break
                }
                if(j == PACKET_START_SIZE) //if last match also done
                {
                    m_currentStatus = PatternStatus::START_FOUND;
                    break; //outer loop break
                }
            }
            m_tail = (m_tail + 1) & (m_capacity - 1);
        }
    }

    if(m_currentStatus == PatternStatus::START_FOUND)
    {
        volatile int16_t sizeToCheck = (m_head - m_tail + m_capacity) & (m_capacity - 1);
        volatile int16_t tempIndex = m_tail + PACKET_START_SIZE;
        for(int i = 0; i < sizeToCheck - PACKET_START_SIZE; i++)
        {
            char ch = m_buffer[tempIndex];
            if (ch == packetEnd[0])
            {
                //first character is matching, match other characters
                int j = 1;
                for(; j < PACKET_END_SIZE; j++)
                {
                    if(m_buffer[tempIndex + j] != packetEnd[j])
                        break;
                }
                if(j == PACKET_END_SIZE) //if last match also done
                {
                    // NOTE// may return from here as well
                    //get the data and put it into somehere
                    if(!commandBuffer.isFull())
                    {
                        AbstractTransfer& nextCommand = commandBuffer.nextCommand();
                        volatile int16_t startIndex = (m_tail + PACKET_START_SIZE) & (m_capacity -1);
                        nextCommand.loadFromBytes(m_buffer, startIndex,
                                                  (tempIndex - startIndex) & (m_capacity -1), m_capacity);
                        commandBuffer.appendCommand(nextCommand);
                    }
                    m_tail = (tempIndex + PACKET_END_SIZE) & (m_capacity - 1);
                    m_currentStatus = PatternStatus::END_FOUND;
                    break;
                }
            }
            else if(ch == packetStart[0]) //if it matches with start
            {
                //first character is matching, match other characters
                int j = 1;
                for(; j < PACKET_START_SIZE; j++)
                {
                    if(m_buffer[tempIndex + j] != packetStart[j])
                        break; //inner loop break
                }
                if(j == PACKET_START_SIZE) //if last match also done
                {
                    // NOTE ERROR//
                    //2nd start witout end break here
                    m_tail = (tempIndex + PACKET_START_SIZE) & (m_capacity - 1);
                    m_currentStatus = PatternStatus::MULTIPLE_START;
                    break; //outer loop break
                }
            }

            tempIndex = (tempIndex + 1) & (m_capacity - 1);
        }
    }

    //check for second start in case some error
    if (m_currentStatus == PatternStatus::END_FOUND ||
            m_currentStatus == PatternStatus::MULTIPLE_START)
    {
        m_currentStatus = PatternStatus::START_NOT_FOUND;
        if (((m_head - m_tail + m_capacity) & (m_capacity - 1)) > (PACKET_START_SIZE + PACKET_END_SIZE))
        {
            goto findStartPattern;
        }
    }
}

