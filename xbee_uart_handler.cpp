#include "xbee_uart_handler.h"
#include "main.h"
#include "algorithm"
#include "command_receiver.h"

//CommandReceiver * const receiver = CommandReceiver::instance();
extern XbeeUartHandler xbee;
extern CommandReceiver receiver;
/* User can use this section to tailor USARTx/UARTx instance used and associated
   resources */
/* Definition for USARTx clock resources */
#define USARTx                           USART2
#define USARTx_CLK_ENABLE()              __HAL_RCC_USART2_CLK_ENABLE();
#define USARTx_RX_GPIO_CLK_ENABLE()      __HAL_RCC_GPIOA_CLK_ENABLE()
#define USARTx_TX_GPIO_CLK_ENABLE()      __HAL_RCC_GPIOA_CLK_ENABLE()

#define USARTx_FORCE_RESET()             __HAL_RCC_USART2_FORCE_RESET()
#define USARTx_RELEASE_RESET()           __HAL_RCC_USART2_RELEASE_RESET()

/* Definition for USARTx Pins */
#define USARTx_TX_PIN                    GPIO_PIN_2
#define USARTx_TX_GPIO_PORT              GPIOA
#define USARTx_TX_AF                     GPIO_AF7_USART2
#define USARTx_RX_PIN                    GPIO_PIN_3
#define USARTx_RX_GPIO_PORT              GPIOA
#define USARTx_RX_AF                     GPIO_AF7_USART2


#define MAX_TRANSFER_SIZE 8
#define TX_BUFFER_MASK  (TX_BUFFER_SIZE - 1)
/* Definition for USARTx's NVIC */
#define USARTx_IRQn                      USART2_IRQn

__IO uint8_t ch;
UART_HandleTypeDef xbeeHandle;

XbeeUartHandler::XbeeUartHandler()
    : m_txHead(0),
      m_txTail(0),
      m_currentTransferSize(0),
      m_TransferComplete(SET)
{
}

XbeeUartHandler::~XbeeUartHandler()
{

}

void XbeeUartHandler::init()
{
    xbeeHandle.Instance          = USARTx;
    xbeeHandle.Init.BaudRate     = 9600;
    xbeeHandle.Init.WordLength   = UART_WORDLENGTH_8B;
    xbeeHandle.Init.StopBits     = UART_STOPBITS_1;
    xbeeHandle.Init.Parity       = UART_PARITY_NONE;
    xbeeHandle.Init.HwFlowCtl    = UART_HWCONTROL_NONE;
    xbeeHandle.Init.Mode         = UART_MODE_TX_RX;
    xbeeHandle.Init.OverSampling = UART_OVERSAMPLING_16;

    if(HAL_UART_Init(&xbeeHandle) != HAL_OK)
    {
        /* Initialization Error */
        Error_Handler();
    }
}

bool XbeeUartHandler::canTransfer(uint16_t numBytes) const
{
    return ((TX_BUFFER_SIZE - usedBufferSize()) > numBytes);
}

void XbeeUartHandler::addToBuffer(const uint8_t *buff, uint16_t numBytes)
{
    for (int i = 0; i < numBytes; i++)
    {
        txBuffer[m_txHead] = buff[i];
        m_txHead = (m_txHead + 1) & TX_BUFFER_MASK;
    }

    if(numBytes && (m_TransferComplete == SET))
        startTransfer();
}

void XbeeUartHandler::startReceiving()
{
    if(HAL_UART_Receive_IT(&xbeeHandle, (uint8_t*)&ch, 1)!= HAL_OK)
    {
        Error_Handler();
    }
}

int16_t XbeeUartHandler::usedBufferSize() const
{
    return (m_txHead - m_txTail + TX_BUFFER_SIZE) & TX_BUFFER_MASK;
}

void XbeeUartHandler::startTransfer()
{
    int16_t transferSize = usedBufferSize();
    m_currentTransferSize = std::min<int16_t>(transferSize, MAX_TRANSFER_SIZE);
    m_TransferComplete = RESET;
    if (m_txTail > m_txHead)
    {
        int16_t continuousSize = (TX_BUFFER_SIZE - m_txTail);
        m_currentTransferSize = std::min<int16_t>(m_currentTransferSize, continuousSize);
    }

    if(HAL_UART_Transmit_IT(&xbeeHandle, (uint8_t*)&txBuffer[m_txTail], m_currentTransferSize)!= HAL_OK)
    {
        Error_Handler();
    }
}

void HAL_UART_MspInit(UART_HandleTypeDef *huart)
{
    if(huart->Instance == USART2)
    {
        GPIO_InitTypeDef  GPIO_InitStruct;
        /*##-1- Enable peripherals and GPIO Clocks #################################*/
        /* Enable GPIO TX/RX clock */
        USARTx_TX_GPIO_CLK_ENABLE();
        USARTx_RX_GPIO_CLK_ENABLE();

        /* Enable USARTx clock */
        USARTx_CLK_ENABLE();

        /*##-2- Configure peripheral GPIO ##########################################*/
        /* UART TX GPIO pin configuration  */
        GPIO_InitStruct.Pin       = USARTx_TX_PIN;
        GPIO_InitStruct.Mode      = GPIO_MODE_AF_PP;
        GPIO_InitStruct.Pull      = GPIO_PULLUP;
        GPIO_InitStruct.Speed     = GPIO_SPEED_FAST;
        GPIO_InitStruct.Alternate = USARTx_TX_AF;

        HAL_GPIO_Init(USARTx_TX_GPIO_PORT, &GPIO_InitStruct);

        /* UART RX GPIO pin configuration  */
        GPIO_InitStruct.Pin = USARTx_RX_PIN;
        GPIO_InitStruct.Alternate = USARTx_RX_AF;

        HAL_GPIO_Init(USARTx_RX_GPIO_PORT, &GPIO_InitStruct);
        /*##-3- Configure the NVIC for UART ########################################*/
        /* NVIC for USART1 */
        HAL_NVIC_SetPriority(USARTx_IRQn, 1, 0);
        HAL_NVIC_EnableIRQ(USARTx_IRQn);
    }
}

/**
  * @brief UART MSP De-Initialization
  *        This function frees the hardware resources used in this example:
  *          - Disable the Peripheral's clock
  *          - Revert GPIO and NVIC configuration to their default state
  * @param huart: UART handle pointer
  * @retval None
  */
void HAL_UART_MspDeInit(UART_HandleTypeDef *huart)
{
    if(huart->Instance == USART2)
    {
        /*##-1- Reset peripherals ##################################################*/
        USARTx_FORCE_RESET();
        USARTx_RELEASE_RESET();

        /*##-2- Disable peripherals and GPIO Clocks #################################*/
        /* Configure UART Tx as alternate function  */
        HAL_GPIO_DeInit(USARTx_TX_GPIO_PORT, USARTx_TX_PIN);
        /* Configure UART Rx as alternate function  */
        HAL_GPIO_DeInit(USARTx_RX_GPIO_PORT, USARTx_RX_PIN);
    }

}

void HAL_UART_TxCpltCallback(UART_HandleTypeDef *UartHandle)
{
    if(UartHandle->Instance == USART2)
    {
        /* Set transmission flag: transfer complete*/
        xbee.m_TransferComplete = SET;
        xbee.m_txTail = (xbee.m_txTail + xbee.m_currentTransferSize) & TX_BUFFER_MASK;
        if(xbee.usedBufferSize())
            xbee.startTransfer();
    }
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *UartHandle)
{
    if(UartHandle->Instance == USART2)
    {
        receiver.push((uint8_t)ch);
        xbee.startReceiving();
    }
}

void HAL_UART_ErrorCallback(UART_HandleTypeDef *UartHandle)
{
    if(UartHandle->Instance == USART2)
    {
        printf("XBEE error!!!\n");
        xbee.startReceiving();
    }
}
