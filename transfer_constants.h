#ifndef TRANSFER_CONSTANTS
#define TRANSFER_CONSTANTS

#include <cstdint>

enum class TransferType: std::uint8_t {
    METHOD_CALL,
    RESPONSE,
    SIGNAL
};

enum class TransferPriority: std::uint8_t {
    URGENT,
    HIGH,
    ABOVE_NORMAL,
    NORMAL,
    LOW,
    BELOW_LOW
};

enum class METHODC_CALL_TYPE: std::uint8_t {
    SET_SPEED, //it will cover the all kind of motion
    SET_DIRECTION
};

// response will have id for the method call so no need of enums for respons
// but we need error code for that

enum class RESPONSE_TYPE: std::uint8_t {
    SUCCESS,
    ERROR //if error return the erro code as well later we will define error codes ;)
};

enum class SIGNAL_TYPE: std::uint8_t {
};

enum class Direction: std::uint8_t {
    Up,
    Left,
    Right,
    Down,
    None
};

#endif // TRANSFER_CONSTANTS

