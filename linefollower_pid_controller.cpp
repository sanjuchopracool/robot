#include "linefollower_pid_controller.h"
#include "motor_driver.h"
extern MotorDriver motorDriver;

constexpr float valueOnWhite = 300.0; //max
constexpr float valueOnBlack = 3700.0; //min
constexpr float diff = (valueOnBlack - valueOnWhite);

LineFollowerPIDController::LineFollowerPIDController() :
    m_prop(0), m_diff(0), m_integral(0), sum(0)
{

}

void LineFollowerPIDController::updateValues(uint16_t *values)
{
    //validate values first!! in case line is not on any sensor use the previous values
    //              -3          -1          1           3        5
    // white 250
    // black atleast 3500
    bool invalidData = true;
    for( uint8_t i = 0; i < 5; i++)
    {
        if (values[i] >= valueOnBlack)
        {
            invalidData = false;
            break;
        }
    }

    if(!invalidData) //black line is somewhere on sensors
    {
        float currentVal = 0;
        for( uint8_t i = 0; i < 5; i++)
        {
            float sensorValFactor = ((float)values[i] - valueOnWhite) / diff;
//            printf("%i : %f\n", i, sensorValFactor);
            if(sensorValFactor < 0.75)
                continue; //right now only digital

            sensorValFactor = 1.0;
            switch (i) {
            case 0:
                sensorValFactor *= -3;
                break;
            case 1:
                sensorValFactor *= -1;
                break;
            case 3:
                sensorValFactor *=  3;
                break;
            case 4:
                sensorValFactor *=  5;
                break;
            default:
                break;
            }

            currentVal += sensorValFactor;
        }

        float error = (currentVal - 1.0) / 4.0;
//        sum += error;
        if(currentVal >= 0) //move right side
        {
            motorDriver.setMotorSpeeds(70.0 , 70.0*(1 - error));
        }
        else //move left side
        {
            motorDriver.setMotorSpeeds(70.0*(1 + error), 70);
        }
    }
}

