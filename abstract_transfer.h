#ifndef ABSTRACTTRANSFER_H
#define ABSTRACTTRANSFER_H

#include "transfer_constants.h"

#define MAX_DATA_SIZE 32

struct AbstractTransferData
{
    AbstractTransferData()
        : type(TransferType::METHOD_CALL),
          priority(TransferPriority::NORMAL),
          dataLength(0),
          id(0)
    { }

    TransferType type;
    TransferPriority priority;
    uint8_t internalType;
    uint8_t dataLength;
    uint32_t id;
};
class AbstractTransfer
{
public:
    AbstractTransfer();
    void loadFromBytes(uint8_t* m_data, uint16_t startIndex, uint8_t size, uint32_t bufferSize); //from bytes copy the data
    virtual ~AbstractTransfer();

    void setId(uint32_t);
    uint32_t id() const;

    void setDataLength(uint32_t);
    uint32_t dataLength() const;

    void setTransferType(TransferType);
    TransferType transferType() const;

    void setPriority(TransferPriority);
    TransferPriority priority() const;

    void setMethodType(METHODC_CALL_TYPE);
    METHODC_CALL_TYPE methodCallType() const;

    void setInternalCall(uint8_t);
    uint8_t internalCall() const;

    void setData(void* data, uint8_t size);
    void transfer();
    void respond(); //in case got method all or signal
    void execute(); //got response containg the data then use response and act

private:
    AbstractTransferData d;
    uint8_t m_data[MAX_DATA_SIZE];
    uint8_t m_size;
};

#endif // ABSTRACTTRANSFER_H
